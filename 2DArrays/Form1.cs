﻿using System;
using System.Windows.Forms;

namespace _2DArrays
{
    public partial class Form1 : Form
    {
        private int index = 0;
        private int x = 0;
        private int[,] array = new int[10, 10];
        private string[] strArr = new string[10];

        public Form1()
        {
            InitializeComponent();
        }

        private void btnBereken_Click(object sender, EventArgs e)
        {
            
            //kolommen
            for (int i = 0; i < array.GetLength(0); i++)
            {
                //rijen
                for (int j = 0; j < array.GetLength(1); j++)
                {
                    if (j < index)
                    {
                        array[i, j] = x;
                        strArr[i] = strArr[i] + " " + x.ToString().PadLeft(10);
                    }
                    else
                    {
                        strArr[i] = strArr[i] + " " + 0.ToString().PadLeft(10);
                    }
                }
            }

            foreach (var item in strArr)
            {
                lbBox1.Items.Add(item);
            }
        }

        private void btnSluiten_Click(object sender, EventArgs e)
        {

            this.Close();

        }

        private void lbBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void txtbox1_TextChanged(object sender, EventArgs e)
        {
            index = Convert.ToInt32(txtbox1.Text);

            if (index > 10)
            {
                txtBox2.ReadOnly = true;

                MessageBox.Show("Getal moet kleiner of gelijk aan 10 zijn!", " Foutmelding!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                txtBox2.ReadOnly = false;
            }
        }

        private void txtBox2_TextChanged(object sender, EventArgs e)
        {
            x = Convert.ToInt32(txtBox2.Text);
        }

        private void label1_Click(object sender, EventArgs e)
        {
        }

        private void label2_Click(object sender, EventArgs e)
        {
        }
    }
}