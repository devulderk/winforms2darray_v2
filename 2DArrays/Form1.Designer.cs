﻿namespace _2DArrays
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSluiten = new System.Windows.Forms.Button();
            this.btnBereken = new System.Windows.Forms.Button();
            this.lbBox1 = new System.Windows.Forms.ListBox();
            this.txtbox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBox2 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnSluiten
            // 
            this.btnSluiten.Location = new System.Drawing.Point(62, 334);
            this.btnSluiten.Name = "btnSluiten";
            this.btnSluiten.Size = new System.Drawing.Size(75, 23);
            this.btnSluiten.TabIndex = 0;
            this.btnSluiten.Text = "Sluiten";
            this.btnSluiten.UseVisualStyleBackColor = true;
            this.btnSluiten.Click += new System.EventHandler(this.btnSluiten_Click);
            // 
            // btnBereken
            // 
            this.btnBereken.Location = new System.Drawing.Point(153, 334);
            this.btnBereken.Name = "btnBereken";
            this.btnBereken.Size = new System.Drawing.Size(75, 23);
            this.btnBereken.TabIndex = 1;
            this.btnBereken.Text = "Berekenen";
            this.btnBereken.UseVisualStyleBackColor = true;
            this.btnBereken.Click += new System.EventHandler(this.btnBereken_Click);
            // 
            // lbBox1
            // 
            this.lbBox1.FormattingEnabled = true;
            this.lbBox1.Location = new System.Drawing.Point(62, 142);
            this.lbBox1.Name = "lbBox1";
            this.lbBox1.Size = new System.Drawing.Size(507, 186);
            this.lbBox1.TabIndex = 2;
            this.lbBox1.SelectedIndexChanged += new System.EventHandler(this.lbBox1_SelectedIndexChanged);
            // 
            // txtbox1
            // 
            this.txtbox1.Location = new System.Drawing.Point(62, 35);
            this.txtbox1.Name = "txtbox1";
            this.txtbox1.Size = new System.Drawing.Size(166, 20);
            this.txtbox1.TabIndex = 3;
            this.txtbox1.TextChanged += new System.EventHandler(this.txtbox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hoeveel indexen wil je verticaal opvullen?";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Welke waarde wil je toevoegen?";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtBox2
            // 
            this.txtBox2.Location = new System.Drawing.Point(62, 90);
            this.txtBox2.Name = "txtBox2";
            this.txtBox2.ReadOnly = true;
            this.txtBox2.Size = new System.Drawing.Size(166, 20);
            this.txtBox2.TabIndex = 5;
            this.txtBox2.TextChanged += new System.EventHandler(this.txtBox2_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 443);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtbox1);
            this.Controls.Add(this.lbBox1);
            this.Controls.Add(this.btnBereken);
            this.Controls.Add(this.btnSluiten);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSluiten;
        private System.Windows.Forms.Button btnBereken;
        private System.Windows.Forms.ListBox lbBox1;
        private System.Windows.Forms.TextBox txtbox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBox2;
    }
}

